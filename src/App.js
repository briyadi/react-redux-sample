import logo from './logo.svg';
import './App.css';
import ButtonAdd from './components/ButtonAdd';
import { useSelector } from 'react-redux';
import { useEffect } from 'react';
import ButtonRemove from './components/ButtonRemove';
import ButtonLoad from './components/ButtonLoad';

function App() {
  const products = useSelector(state => state.products)

  useEffect(() => {
    console.log(products)
  }, [products])

  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <p>
          Edit <code>src/App.js</code> and save to reload.
        </p>
        <a
          className="App-link"
          href="https://reactjs.org"
          target="_blank"
          rel="noopener noreferrer"
        >
          Learn React
        </a>
        <ButtonAdd />
        <ButtonRemove />
        <ButtonLoad />
      </header>
    </div>
  );
}

export default App;
