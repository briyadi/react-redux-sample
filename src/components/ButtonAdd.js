import { useDispatch } from "react-redux"
import { ADD_PRODUCT } from "../redux/actions"

export default function ButtonAdd() {
    const dispatch = useDispatch()

    const addProduct = () => {
        dispatch({
            type: ADD_PRODUCT,
            payload: {
                id: 2,
                name: "Mie instant",
                price: 5000,
            }
        })
    }

    return (
        <button onClick={addProduct}>Add product</button>
    )
}