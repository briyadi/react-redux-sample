import { useDispatch } from "react-redux"
import { loadAllProducts } from "../redux/reducers/product_thunk"

export default function ButtonLoad() {
    const dispatch = useDispatch()

    const loadProduct = () => {
        // loadAllProducts = new Promise()
        dispatch(loadAllProducts())
    }

    return (
        <button onClick={loadProduct}>Load Product</button>
    )
}