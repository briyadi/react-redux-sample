import { useDispatch } from "react-redux"
import { REMOVE_PRODUCT } from "../redux/actions"

export default function ButtonRemove() {
    const dispatch = useDispatch()

    const removeProduct = () => {
        dispatch({
            type: REMOVE_PRODUCT,
            payload: 1
        })
    }

    return (
        <button onClick={removeProduct}>Remove Product</button>
    )
}