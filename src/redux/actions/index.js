export const ADD_PRODUCT = "product/ADD"
export const REMOVE_PRODUCT = "product/REMOVE"
export const LOAD_PRODUCT = "product/LOAD"