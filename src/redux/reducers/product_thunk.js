import { LOAD_PRODUCT } from "../actions"

export function loadAllProducts() {
    return async function(dispatch, getState) {
        let response = await fetch("https://dummyjson.com/products")
        let data = await response.json()
        dispatch({
            type: LOAD_PRODUCT,
            payload: [...data.products]
        })
    }
}