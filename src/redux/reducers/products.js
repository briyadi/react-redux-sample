import { ADD_PRODUCT, LOAD_PRODUCT, REMOVE_PRODUCT } from "../actions"

const initialState = [
    {
        id: 1,
        name: "Air Mineral",
        price: 10000,
    }
]

export default function Products(state = initialState, action) {
    switch (action.type) {
        case ADD_PRODUCT:
            let newProduct = action.payload
            newProduct.id = state.length + 1
            
            return [...state, newProduct]
        case REMOVE_PRODUCT:
            return state.filter(item => item.id !== action.payload)
        case LOAD_PRODUCT:
            return [...action.payload]
        default:
            return state
    }
}